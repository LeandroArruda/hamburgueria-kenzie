import {Component} from 'react'

class Product extends Component {
    render() {
        return(
            <div>
                    <h1>{this.props.obj.name}</h1>
                    <h2>Categoria: {this.props.obj.category}</h2>
                    <h3>Preço: {this.props.obj.price}</h3>
            </div>
        )
    }
}

export default Product