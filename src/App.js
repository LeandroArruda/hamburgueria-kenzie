// import logo from './logo.svg';
import './App.css';
import {Component} from 'react'
import MenuContainer from './components/MenuContainer'
import Product from './components/Product'

class App extends Component {
  state = {
    products: [
      { id: 1, name: 'Hamburguer', category: 'Sanduíches', price: 7.99 },
      { id: 2, name: 'X-Burguer', category: 'Sanduíches', price: 8.99 },
      { id: 3, name: 'X-Salada', category: 'Sanduíches', price: 10.99 },
      { id: 4, name: 'Big Kenzie', category: 'Sanduíches', price: 16.99 },
      { id: 5, name: 'Guaraná', category: 'Bebidas', price: 4.99 },
      { id: 6, name: 'Coca', category: 'Bebidas', price: 4.99 },
      { id: 7, name: 'Fanta', category: 'Bebidas', price: 4.99 },
    ],
    filteredProducts: [
      { id: 1, name: 'Hamburguer', category: 'Sanduíches', price: 7.99 },
      { id: 2, name: 'X-Burguer', category: 'Sanduíches', price: 8.99 },
      { id: 3, name: 'X-Salada', category: 'Sanduíches', price: 10.99 },
      { id: 4, name: 'Big Kenzie', category: 'Sanduíches', price: 16.99 },
      { id: 5, name: 'Guaraná', category: 'Bebidas', price: 4.99 },
      { id: 6, name: 'Coca', category: 'Bebidas', price: 4.99 },
      { id: 7, name: 'Fanta', category: 'Bebidas', price: 4.99 },],
    currentSale: { total: 0, saleDetails: [] },
    value: '',
    showSale: true
    }

  showProducts = () => {
    console.log(this.state.value)
    if(this.state.value === ''){
      this.setState({filteredProducts:this.state.products})
    }
    else
    this.setState({filteredProducts:this.state.products.filter(e=>e.name === this.state.value)})
    return 
  }

  handleClick = (event) => {
    const item = this.state.products.find(e=>e.id === parseInt(event.target.id))
    this.setState({currentSale:{total: 0, saleDetails: [...this.state.currentSale.saleDetails, item]}})
  }

  showSale = () => {
    this.state.showSale? this.setState({showSale: false}) : this.setState({showSale: true})
  }

  render() {
    const jsx = <p>{this.state.currentSale.saleDetails.reduce((acc,e)=>acc+=e.price,0)}</p>
    return (
      <div>
        <input value={this.state.value} onChange={(event) => this.setState({value: event.target.value})}></input>
        <button onClick={this.showProducts}>Pesquisar</button>
        <div className='cardapio'>
        <MenuContainer array={this.state.filteredProducts}  click={this.handleClick} haveButton show /> 
        </div>
        <p>Subtotal: {jsx} </p>
        <button onClick={this.showSale}>{this.state.showSale? 'Hide sale': 'Show sale'}</button>
        <MenuContainer array={this.state.currentSale.saleDetails} show={this.state.showSale}/>
      </div>
    )
  }
}

export default App;
